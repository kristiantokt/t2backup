from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class AppHomeUnitTest(TestCase):
    def test_app_home_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_app_home_using_index_func(self):
        found = resolve('/home/')
        self.assertEqual(found.func, index)