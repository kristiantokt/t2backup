from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt
from .forms import Forum_Lowongan
from .models import Forum


# Create your views here.
response = {}
def index(request):
    forum_list = Forum.objects.all()
    forum_latest = forum_list[::-1]
    page = request.GET.get('page', 1)
    paginator = Paginator(forum_latest, 5)
    try:
        forum = paginator.page(page)
    except PageNotAnInteger:
        forum = paginator.page(1)
    except EmptyPage:
        forum = paginator.page(paginator.num_pages)
    response['forum'] = forum
    response['forum_lowongan'] = Forum_Lowongan
    html = 'app_forum/app_forum.html'
    return render(request, html, response)

@csrf_exempt
def add_forum(request):
    if request.method == 'POST':
        company_id = request.POST['company_id']
        lowongan = request.POST['lowongan']
        forum = Forum(company_id=company_id,lowongan=lowongan)
        forum.save()
        return HttpResponseRedirect('/forum/')
    else:
        return HttpResponseRedirect('/forum/')