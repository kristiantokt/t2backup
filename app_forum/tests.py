from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class AppHomeUnitTest(TestCase):
    def test_app_home_url_is_exist(self):
        response = Client().get('/forum/')
        self.assertEqual(response.status_code, 200)

    def test_app_home_using_index_func(self):
        found = resolve('/forum/')
        self.assertEqual(found.func, index)

    def test_paginate_exception_not_an_integer(self):
        response = Client().get('/forum/?page=this')
        self.assertEqual(response.status_code, 200)

    def test_paginate_exception_empty_data(self):
        response = Client().get('/forum/?page=11')
        self.assertEqual(response.status_code, 200)

    def test_add_forum_is_done(self):
        response_post = Client().post('/forum/add_forum/', {'company_id': "1234", 'lowongan': "Kami membuka lowongan"})
        self.assertEqual(response_post.status_code, 302)

    def test_add_forum_is_failed_because_get_client(self):
        response_post = Client().get('/forum/add_forum/?company_id=1234&lowongan=kami/')
        self.assertEqual(response_post.status_code, 302)