import requests

API_LINKEDIN = "https://api.linkedin.com/v1/companies/"
COMPANY_ID = "13600560"

def get_company_profile():
    try:
        url = API_LINKEDIN + COMPANY_ID + ":(id,name,description,website-url,logo-url,founded-year,specialties)?format=json"
        headers = {
            'authorization': "Bearer AQVpEeaSXBRUsgcYdgPosz8h84N1i7_NSLAx_GfG_3Gv7hBTOzNzgizSnvngKmAxj6xec3Jxed-u-u0jKfd1zdAvZrdKQ5pnVOqAKHB4pDaSn2f8rZROfhD0eD5B-722o2UlwieP337SklFcjdTX1AlF2UsxMQ27Q6naGZf1RdKoXd3uG42lc9jrelQzDoLiTXyfFFjEnU-zkYkfiic9MPoM9XwpYBsrC91wtqOF3j9EL3CTowD65fPWFhgkk_Puv8U9RkvgYuUJyPM5p2Sc-pRqd6Aq7Wt_Ps56wvXoTc-NjfGqnkqptInb5ohoM2IxF_IwkoDGWKPunocNevBdVZAB3mgFPQ"
        }
        response = requests.get(url, headers=headers)
        print(response.json())
        return response.json()
    except Exception:
        raise Exception("can't connect to LinkedIn")
